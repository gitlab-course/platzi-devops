import React from 'react';
import logo from './logo.svg';
import './App.css';
import Playground from './components/Playground';


function App() {
  return (
    <Playground />
  );
}

export default App;
